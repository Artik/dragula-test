// angular/components/quickstart/QuickstartCtrl.js

define(['dragula'], function() {
	'use strict';

	function QuickstartCtrl($scope, $rootScope, dragulaService) {

		$scope.days = "Пн,Вт,Ср,Чт,Пт,Сб,Вс".split(',');
		$scope.times = [];
		for (var i = 0; i < 24; i++) {
			if (i < 10) i = "0" + i;
			$scope.times.push(i.toString())
		}

		$scope.scheduler = [[], [], [], [], [], [], []];

		$scope.savedTemplates = [];

		$scope.bottomMenu = [];

		var createTemplate = function(id) {
			return {
				id: id,
				name: 'template-' + id
			}
		};

		var templateIndex = 0;
		for (var i = 0; i < Math.random() * 10 + 5; i++) {
			templateIndex++;
			var template = createTemplate(templateIndex);
			$scope.savedTemplates.push(template);
		}

		// $scope.scheduler.forEach(function(day) {
		// 	for (var i = 0; i < Math.random() * 10; i++) {
		// 		var template = $scope.savedTemplates[(Math.floor(Math.random() * $scope.savedTemplates.length))];
		// 		day.push(template);
		// 	}
		// });

		$scope.$on('bag-template.drop', function() {
			$rootScope.$broadcast('template:remove-states');
		});

		var plateResize = false,
			resizeTop = false,
			plate = null,
			plateSize = 0,
			plateScope = {};
		var lastCords = {
			x: 0,
			y: 0
		};
		dragulaService.options($scope, 'bag-template', {
			copy: function(item, source) {
				return angular.element(source).attr('dragula-behavior').indexOf('copy') >= 0;
			},
			accepts: function(item, source) {
				return angular.element(source).attr('dragula-behavior').indexOf('get') >= 0;
			},
			moves: function(el, source, handle, sibling) {
				var hnd = angular.element(handle),
					isT = hnd.hasClass('time'),
					isR = hnd.hasClass('resizer');
				var isTop = false;
				var isResizer = isT || isR;
				if (isResizer) {
					plateResize = true;
					plate = el;
					plateScope = angular.element(el).scope();
					plateSize = el.offsetHeight;
					if (isT) {
						isTop = hnd.parent().hasClass('resizer-top');
					} else {
						isTop = hnd.hasClass('resizer-top');
					}
					resizeTop = isTop;
				}
				return !isResizer;
			}
		});

		angular.element(document)
			.on('mousemove', function(e) {
				if (plateResize) {
					var newCords = {
						x: e.pageX || e.changedTouches[0].pageX,
						y: e.pageY || e.changedTouches[0].pageY
					};
					var newHeight = plateSize;
					var d = (newCords.y - lastCords.y);

					if (resizeTop) {
						newHeight = plateSize - d;
					} else {
						newHeight = plateSize + d;
					}

					var scale = 100 / angular.element(plate).parent()[0].offsetHeight;

					plate.style.height = newHeight * scale + '%';
					plateScope.template.height = newHeight * scale;
				}
			})
			.on('mousedown', function(e) {
				lastCords.x = e.pageX || e.changedTouches[0].pageX;
				lastCords.y = e.pageY || e.changedTouches[0].pageY;
			})
			.on('mouseup', function(e) {
				plateResize = false;
			});

	}

	return {
		QuickstartCtrl: [
			'$scope', '$rootScope', 'dragulaService', QuickstartCtrl
		]
	};
});