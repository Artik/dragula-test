// angular/app.modules.js

define([
	'dragula'
], function(dragula) {
	'use strict';
	return {
		'dragula': dragula
	};
});