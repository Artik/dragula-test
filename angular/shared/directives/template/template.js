define([], function() {
	'use strict';

	function template($rootScope) {

		return {
			restrict: 'A',
			replace: true,
			templateUrl: 'angular/shared/directives/template/template.html',
			link: function(scope, element, attrs) {

				scope.template.active = false;
				scope.template.inactive = false;
				scope.template.time_start = 0;
				scope.template.time_end = 0;
				scope.template.time_start_str = "00:00";
				scope.template.time_end_str = "00:00";

				scope.templateActive = function(template) {
					if(!template){
						console.log(scope.template)
						return false;
					}
					if (!template.active) {
						$rootScope.$broadcast('template:active');
						scope.template.inactive = false;
					} else {
						$rootScope.$broadcast('template:inactive');
					}
					scope.template.active = !scope.template.active;
				};

				$rootScope.$on('template:active', function() {
					scope.template.active = false;
					scope.template.inactive = true;
				});

				$rootScope.$on('template:inactive', function() {
					scope.template.inactive = false;
				});

				$rootScope.$on('template:remove-states', function() {
					scope.template.active = false;
					scope.template.inactive = false;
				});
			}
		};
	}

	return {
		template: [
			'$rootScope', template
		]
	};
});