// angular/app.js

define([
	'angular',
	'./app.namespace',
	'./app.modules',
	'./app.controllers',
	'./app.directives'

], function(ng, namespace, modules, controllers, directives) {
	'use strict';

	ng.module(namespace, [
		modules.dragula(ng)
	]);

	ng.forEach(controllers, function(item, name) {
		ng.module(namespace)
			.controller(name, item);
	});

	ng.forEach(directives, function(item, name) {
		ng.module(namespace)
			.directive(name, item);
	});

});
