// angular/main.js

require.config({
	baseUrl: './angular',
	paths: {
		'angular': '../bower_components/angular/angular',
		'dragula': '../bower_components/angular-dragula/dist/angular-dragula'
	},
	shim: {
		'angular': {
			deps: [],
			exports: 'angular'
		}
	}
});

require(['angular', './app', './app.namespace', './app.modules'], function(ng, app, namespace) {
	'use strict';

	ng.element(document).ready(function() {
		ng.bootstrap(document, [namespace]);
	});
});